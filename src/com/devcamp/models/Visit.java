package com.devcamp.models;

import java.util.Date;

public class Visit {
 private Customer customer;
 private Date date;
 private double serviceExpense;
 private double productExpense;

 public Visit(Customer customer, Date date){
  this.customer = customer;
  this.date = date;
 }

 public String getName() {
  return customer.getName();
 }

 public double getServiceExpense() {
  return serviceExpense;
 }

 public void setServiceExpense(double serviceExpense) {
  this.serviceExpense = serviceExpense;
 }

 public double getProductExpense() {
  return productExpense;
 }

 public void setProductExpense(double productExpense) {
  this.productExpense = productExpense;
 }

 public double getTotalExpense() {
  return productExpense + serviceExpense;
 }

 @Override
 public String toString() {
  
  return String.format("Visit[ %s, date = %s, serviceExpense = %s , productExpense = %s", customer , date, serviceExpense, productExpense);

}
}
